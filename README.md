# Angular 微前端实现方法 - Angular Micro Frontends

## 说明

- **此项目仅用于向中文开发者演示 Angular 微前端实现方法，所有权全部归属于原作者。This project is only used to demonstrate the Angular micro-frontend implementation method to Chinese developers, and all ownership belongs to the original author.**
- **此项目要求 Angular 和 @angular/cli 的版本>=14 ，老版本的项目需要先升级 @angular/cli 到 v14 以上。**
- 基于 @angular-architects/module-federation 工具模块实现。
- 在原有的例子基础上，修改了一些代码，增加了部署到 nginx 的说明和配置文件示例。

英文原版相关资料参见：
- @angular-architects/module-federation 的 npm 包：https://www.npmjs.com/package/@angular-architects/module-federation
- 英文原版说明文章：https://dev.to/seanperkins/angular-dynamic-modules-at-runtime-with-module-federation-mk5
- @angular-architects/module-federation 工具模块的 github repo：https://github.com/angular-architects/module-federation-plugin

## 用法

请在终端中执行：

    git clone https://gitee.com/mumu-osc/module-federation-plugin-example.git
    cd module-federation-plugin-example
    nrm use taobao
    npm install
    npm run start:shell
    npm run start:mfe1

<img src="./ng-mfe.png">

- 壳子项目 shell 运行在 8090 端口。
- 被集成的子项目 mfe1 运行在 8091 端口。
- 核心的配置文件在两个子项目的 webpack.config.js 中。

## 部署到 nginx

1. npm run build 构建2个工程
1. 请参考 nginx-8090.conf 和 nginx-8091.conf 中的配置，将 shell 和 mfe1 两个项目打包之后的静态资源部署到不同的 nginx 实例上（注意：关键点是两份配置配置文件中都开启了允许跨域。）
1. 启动2台 nginx 实例
1. 分别访问两台 nginx 上部署的应用，可以看到两个应用都正常运行， shell 应用可以加载来自于 mfe1 应用的组件，效果与上一副图片相同。

**到这里为止，就实现了 Angular 项目的微前端化了，各个项目工程可以独立访问，也可以加载来自于其它项目的模块。**

## License

MIT

